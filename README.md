# Backend for Todolist on Nest.js  

### Swagger API

[http://localhost:3001/api/docs](http://localhost:3001/api/docs)

### Installation

```bash
$ yarn install
```

### Run

```bash
$ yarn start:dev
$ yarn start:prod
```

### Test (in work)

```bash
$ yarn test
$ yarn test:e2e
$ yarn test:cov
```

### Configs

```bash
# for docker should be POSTGRES_HOST=postgres
$ cat .env.testing
$ cat .env.production
```

### Docker

```bash
$ docker-compose up
# curl -v -XGET http://localhost:3001/tasks
# psql -hlocalhost -p5432 -Upostgres
```