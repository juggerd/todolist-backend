FROM node:16.14-alpine3.15

WORKDIR /app

COPY package*.json ./

RUN npm install -g yarn; yarn install

COPY . .

COPY ./dist ./dist

CMD ["yarn", "start:dev"]