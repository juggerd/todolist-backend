import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Task } from './tasks.model';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';

@Injectable()
export class TasksService {
  constructor(@InjectModel(Task) private taskRepository: typeof Task) {}

  async createTask(dto: CreateTaskDto) {
    return await this.taskRepository.create(dto);
  }

  async updateTask(id: number, dto: UpdateTaskDto) {
    return await this.taskRepository.update(
      {
        title: dto.title,
        description: dto.description,
      },
      {
        where: {
          id: id,
        },
      },
    );
  }

  async deleteTask(id: number) {
    return await this.taskRepository.destroy({
      where: {
        id: id,
      },
    });
  }

  async getAllTasks() {
    return await this.taskRepository.findAll();
  }
}
