import {
  Body,
  Controller,
  Post,
  Get,
  UsePipes,
  Delete,
  Param,
  Put,
} from '@nestjs/common';
import { CreateTaskDto } from './dto/create-task.dto';
import { TasksService } from './tasks.service';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Task } from './tasks.model';
import { ValidationPipe } from '../pipes/validation.pipe';
import { UpdateTaskDto } from './dto/update-task.dto';

@ApiTags('Tasks')
@Controller('tasks')
export class TasksController {
  constructor(private tasksService: TasksService) {}

  @ApiOperation({ summary: 'Create task' })
  @ApiResponse({ status: 200, type: Task })
  @Post()
  @UsePipes(ValidationPipe)
  create(@Body() dto: CreateTaskDto) {
    return this.tasksService.createTask(dto);
  }

  @ApiOperation({ summary: 'Update task' })
  @ApiResponse({ status: 200 })
  @Put(':id')
  @UsePipes(ValidationPipe)
  update(@Param() params, @Body() dto: UpdateTaskDto) {
    this.tasksService.updateTask(params.id, dto);
  }

  @ApiOperation({ summary: 'Delete task' })
  @ApiResponse({ status: 200 })
  @Delete(':id')
  delete(@Param() params) {
    this.tasksService.deleteTask(params.id);
  }

  @ApiOperation({ summary: 'Get all tasks' })
  @ApiResponse({ status: 200, type: [Task] })
  @Get()
  getAll() {
    return this.tasksService.getAllTasks();
  }
}
