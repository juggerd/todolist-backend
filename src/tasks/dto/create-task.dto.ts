import { ApiProperty } from '@nestjs/swagger';
import { IsString, Length } from 'class-validator';

export class CreateTaskDto {
  @ApiProperty({
    required: true,
    minLength: 1,
    description: 'Task title',
  })
  @IsString({ message: 'Must be a string' })
  @Length(1, 255, { message: 'Length must be > 0 and < 256' })
  readonly title: string;

  @ApiProperty({
    required: false,
    description: 'Task description',
  })
  readonly description?: string;
}
