import { Column, Model, Table } from 'sequelize-typescript';
import { DataTypes } from 'sequelize';
import { ApiProperty } from '@nestjs/swagger';

@Table({ tableName: 'tasks' })
export class Task extends Model<Task> {
  @ApiProperty({
    description: 'Task id',
  })
  @Column({
    type: DataTypes.INTEGER,
    unique: true,
    autoIncrement: true,
    primaryKey: true,
  })
  id: number;

  @ApiProperty({
    description: 'Task title',
  })
  @Column({
    type: DataTypes.STRING,
    allowNull: false,
  })
  title: string;

  @ApiProperty({
    description: 'Task description',
  })
  @Column({
    type: DataTypes.STRING,
    defaultValue: null,
  })
  description: string;
}
