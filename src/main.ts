import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const swagger_config = new DocumentBuilder()
    .setTitle('Todolist - backend API')
    .setDescription('Simple REST API for Todolist - frontend')
    .setVersion('0.0.1')
    .build();

  const swagger_document = SwaggerModule.createDocument(app, swagger_config);
  SwaggerModule.setup('/api/docs', app, swagger_document);

  const port = process.env.SERVICE_PORT || 3001;
  await app.listen(port, () => {
    console.log(`Server started on port ${port}`);
  });
}
bootstrap();
